import numpy as np


# two dimension board
class Board:

    def __init__(self, row, column):
        self.row = row
        self.column = column
        self.board = np.chararray(shape=(column, row))
        self.current_x = 0
        self.current_y = 0
        self.start = ()
        self.goal = ()
        self.current = ()

        # each move cost is 1.5 for this game, please change if game is changed
        self.move_cost = 1.5

    # it is constructing the board
    def put_element(self, element):
        self.board[self.current_x, self.current_y] = element
        if element == "S":
            self.start += ((self.current_x, self.current_y),)
            self.current += ((self.current_x, self.current_y),)
        elif element == "G":
            self.goal += ((self.current_x, self.current_y),)
        self.current_x += 1
        if self.current_x == self.column:
            self.current_x = 0
            self.current_y += 1

    def get_element(self, coordinate):
        return self.board[coordinate]

    def get_current(self):
        return self.current

    def get_goal(self):
        return self.goal

    def get_board(self):
        return self.board

    # expand function, return successor states
    def get_next_moves(self, current_loc):
        cl = current_loc
        moves = []
        # if block use 2 square
        if len(cl) == 2:
            # if block is horizontal
            if cl[0][1] == cl[1][1]:
                # left
                if cl[0][0] > 0 and self.board[cl[0][0]-1, cl[0][1]] != 'X':
                    moves.append((((cl[0][0]-1, cl[0][1]),), self.move_cost))
                # right
                if cl[1][0] < self.column - 1 and self.board[cl[1][0]+1, cl[1][1]] != 'X':
                    moves.append((((cl[1][0]+1, cl[1][1]),), self.move_cost))
                # up
                if cl[0][1] > 0 and self.board[cl[0][0], cl[0][1]-1] != 'X' \
                        and self.board[cl[1][0], cl[1][1]-1] != 'X':
                    moves.append((((cl[0][0], cl[0][1]-1), (cl[1][0], cl[1][1]-1)), self.move_cost))
                # down
                if cl[0][1] < self.row - 1 and self.board[cl[0][0], cl[0][1]+1] != 'X' \
                        and self.board[cl[1][0], cl[1][1]+1] != 'X':
                    moves.append((((cl[0][0], cl[0][1]+1), (cl[1][0], cl[1][1]+1)), self.move_cost))
            # if block is vertical
            else:
                # left
                if cl[0][0] > 0 and self.board[cl[0][0]-1, cl[0][1]] != 'X' \
                        and self.board[cl[1][0]-1, cl[1][1]] != 'X':
                    moves.append((((cl[0][0]-1, cl[0][1]), (cl[1][0]-1, cl[1][1])), self.move_cost))
                # right
                if cl[0][0] < self.column - 1 and self.board[cl[0][0]+1, cl[0][1]] != 'X' \
                        and self.board[cl[1][0]+1, cl[1][1]] != 'X':
                    moves.append((((cl[0][0]+1, cl[0][1]), (cl[1][0]+1, cl[1][1])), self.move_cost))
                # up
                if cl[0][1] > 0 and self.board[cl[0][0], cl[0][1]-1] != 'X':
                    moves.append((((cl[0][0], cl[0][1]-1),), self.move_cost))
                # down
                if cl[1][1] < self.row - 1 and self.board[cl[0][0], cl[1][1]+1] != 'X':
                    moves.append((((cl[0][0], cl[1][1]+1),), self.move_cost))
        # if block use 1 square
        else:
            # left
            if cl[0][0] > 1 and self.board[cl[0][0]-2, cl[0][1]] != 'X' \
                    and self.board[cl[0][0]-1, cl[0][1]] != 'X':
                moves.append((((cl[0][0]-2, cl[0][1]), (cl[0][0]-1, cl[0][1])), self.move_cost))
            # right
            if cl[0][0] < self.column - 2 and self.board[cl[0][0]+1, cl[0][1]] != 'X' \
                    and self.board[cl[0][0]+2, cl[0][1]] != 'X':
                moves.append((((cl[0][0]+1, cl[0][1]), (cl[0][0]+2, cl[0][1])), self.move_cost))
            # up
            if cl[0][1] > 1 and self.board[cl[0][0], cl[0][1]-2] != 'X' \
                    and self.board[cl[0][0], cl[0][1]-1] != 'X':
                moves.append((((cl[0][0], cl[0][1]-2), (cl[0][0], cl[0][1]-1)), self.move_cost))
            # down
            if cl[0][1] < self.row - 2 and self.board[cl[0][0], cl[0][1]+1] != 'X' \
                    and self.board[cl[0][0], cl[0][1]+2] != 'X':
                moves.append((((cl[0][0], cl[0][1]+1), (cl[0][0], cl[0][1]+2)), self.move_cost))

        return moves


# read board from txt file
def read_board(file_name):
    file_reader = open(file_name, "r")
    board_txt = file_reader.read()

    board_lines = board_txt.split("\n")
    row = len(board_lines)
    column = len(board_lines[0].split(" "))

    board = Board(row, column)
    for board_line in board_lines:
        words = board_line.split(" ")
        for word in words:
            board.put_element(word)

    return board


# creates board to the file
def create_board(file_name, row, column):
    file_writer = open(file_name, "w")
    for i in range(0, row):
        if i == 0:
            line = 'G'
        else:
            line = 'O'
        for j in range(0, column-1):
            if i == (row - 2)/2 and j == (column - 2)/2:
                line += ' S'
            else:
                line += ' O'
        file_writer.write(line + "\n")


# test
# board = Board(3, 2)
# board.put_element("a")
# board.put_element("s")
# board.put_element("d")
# board.put_element("f")
# board.put_element("g")
# board.put_element("h")
# print(board.get_element(1))

# test for tuple
# a = ()
# a += (3,)
# print(len(a))
# a += (5,)
# print(len(a))
# print(a[1])
