from priority_queue import *
import sys
from util import *


# a* implementation using graph
def a_star(_board, message=False):
    frontier = PriorityQueue()
    max_frontier_size = 0
    closed = {}

    current_loc = _board.get_current()
    goal_loc = _board.get_goal()
    frontier.insert((current_loc, None, 0, 0), 0)
    last_cost = 0

    sys.stdout.write('Block starts from %s and will move to %s\n' % (current_loc, goal_loc))
    sys.stdout.write('Game is started (Using A* Algorithm)!\n')
    sys.stdout.flush()

    while not frontier.is_empty():

        # change the max frontier size for memory comparison
        if frontier.get_size() > max_frontier_size:
            max_frontier_size = frontier.get_size()

        # removing element from frontier, put into closed
        current_cost, _, current_node = frontier.remove()
        current_loc, parent_loc, f_cost, g_cost = current_node
        closed[current_loc] = (parent_loc, f_cost, g_cost)

        if current_loc == goal_loc:
            last_cost = g_cost
            break

        if message:
            print("Block is moving to %s" % (current_loc,))

        # get all next move
        next_moves = _board.get_next_moves(current_loc)
        # next_loc is successor state, cost is cost of that move
        for next_loc, cost in next_moves:
            # calculate heuristic
            h_cost = heuristic_euclidean(next_loc, goal_loc)
            next_g_cost = g_cost + cost
            next_f_cost = h_cost + next_g_cost
            if next_loc not in closed and next_loc not in frontier:
                frontier.insert((next_loc, current_loc, next_f_cost, next_g_cost), next_f_cost)
            # modify frontier if frontier has element with more cumulative cost
            elif next_loc in frontier:
                temp_cost = frontier.get_cost(next_loc)
                if next_f_cost < temp_cost:
                    del frontier[next_loc]
                    frontier.insert((next_loc, current_loc, next_f_cost, next_g_cost), next_f_cost)
            # i have monotone heuristic, i don't need below code.
            # elif next_loc in closed:
            #     temp_parent_loc, temp_f_cost, temp_g_cost = closed[next_loc]
            #     if next_f_cost < temp_f_cost:
            #         frontier.insert((next_loc, temp_parent_loc, temp_f_cost, temp_g_cost), temp_f_cost)
            #         closed[next_loc] = (current_loc, next_f_cost, next_g_cost)

    if current_loc == goal_loc:
        sys.stdout.write('Block reached the goal! Cost: %s\n' % last_cost)
        sys.stdout.write('The Path from starting location to goal location is:\n')
        sys.stdout.flush()
        path = get_path(closed, current_loc)
        # print path
        for location in path:
            sys.stdout.write('%s --> ' % (location,))
        sys.stdout.write('SUCCESS\n')
        sys.stdout.flush()
        print ('Closed size is'), (len(closed))
        print ('Max Frontier size is'), (max_frontier_size)


# ucs implementation using graph
def ucs(_board, message=False):
    frontier = PriorityQueue()
    max_frontier_size = 0
    closed = {}

    current_loc = _board.get_current()
    goal_loc = _board.get_goal()
    frontier.insert((current_loc, None), 0)
    last_cost = 0

    sys.stdout.write('Block starts from %s and will move to %s\n' % (current_loc, goal_loc))
    sys.stdout.write('Game is started! (Using UCS Algorithm)\n')
    sys.stdout.flush()

    while not frontier.is_empty():

        # change the max frontier size for memory comparison
        if frontier.get_size() > max_frontier_size:
            max_frontier_size = frontier.get_size()

        # removing element from frontier, put into closed
        current_cost, _, current_node = frontier.remove()
        current_loc, parent_loc = current_node
        closed[current_loc] = (parent_loc, current_cost)

        if current_loc == goal_loc:
            last_cost = current_cost
            break

        if message:
            print("Block is moving to %s" % (current_loc,))

        # get all next move
        next_moves = _board.get_next_moves(current_loc)
        # next_loc is successor state, cost is cost of that move
        for next_loc, cost in next_moves:
            total_cost = current_cost + cost
            if next_loc not in closed and next_loc not in frontier:
                frontier.insert((next_loc, current_loc), total_cost)
            elif next_loc in frontier:
                temp_cost = frontier.get_cost(next_loc)
                # modify frontier if frontier has element with more cumulative cost
                if total_cost < temp_cost:
                    del frontier[next_loc]
                    frontier.insert((next_loc, current_loc), total_cost)

    if current_loc == goal_loc:
        sys.stdout.write('Block reached the goal! Cost: %s\n' % last_cost)
        sys.stdout.write('The Path from starting location to goal location is:\n')
        sys.stdout.flush()
        path = get_path(closed, current_loc)
        # print path
        for location in path:
            sys.stdout.write('%s --> ' % (location,))
        sys.stdout.write('SUCCESS\n')
        sys.stdout.flush()
        print ('Closed size is'), (len(closed))
        print ('Max Frontier size is'), (max_frontier_size)
