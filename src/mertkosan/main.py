import time
import search
from board import *


def main():
    board = read_board("../../res/stage_mert.txt")

    start = time.time()
    search.ucs(board, False)
    end = time.time()
    print('Execution time: '), (end - start)

    print('--------------------------------------------------------------------------------------------')
    print('--------------------------------------------------------------------------------------------')

    start = time.time()
    search.a_star(board, False)
    end = time.time()
    print('Execution time: '), (end - start)


def main2():
    create_board("../../res/stage_mert2.txt", 200, 200)

if __name__ == '__main__':
    main()
    # main2()
