import math


# heuristic function, euclidean
# if block covers one square, uses coordinate
# if block covers two squares, uses avg of coordinates
def heuristic_euclidean(current_loc, goal_loc):
    if len(current_loc) == 1:
        a = abs(current_loc[0][0] - goal_loc[0][0])
        b = abs(current_loc[0][1] - goal_loc[0][1])
        return math.hypot(a, b)
    else:
        a1 = abs(current_loc[0][0] - goal_loc[0][0])
        b1 = abs(current_loc[0][1] - goal_loc[0][1])
        a2 = abs(current_loc[1][0] - goal_loc[0][0])
        b2 = abs(current_loc[1][1] - goal_loc[0][1])
        return math.hypot((a1+a2)/2, (b1+b2)/2)


# heuristic function, manhattan
# if block covers one square, return coordinate
# if block covers two squares, return avg of coordinates
def heuristic_manhattan(current_loc, goal_loc):
    if len(current_loc) == 1:
        a = abs(current_loc[0][0] - goal_loc[0][0])
        b = abs(current_loc[0][1] - goal_loc[0][1])
        return a + b
    else:
        a1 = abs(current_loc[0][0] - goal_loc[0][0])
        b1 = abs(current_loc[0][1] - goal_loc[0][1])
        a2 = abs(current_loc[1][0] - goal_loc[0][0])
        b2 = abs(current_loc[1][1] - goal_loc[0][1])
        return ((a1+a2)/2) + ((b1+b2)/2)


# it returns result path
def get_path(closed, current_loc):
    path = []
    while closed[current_loc][0]:
        path.append(current_loc)
        current_loc = closed[current_loc][0]
    path.append(current_loc)
    return path[::-1]
