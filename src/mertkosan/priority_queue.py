# encoding:utf-8

# This file implements a priority queue using heapq module
# Author: Marcos Castro
# Modified by: mertkosan
# GitHub Link: https://github.com/marcoscastro/ucs


import heapq


class PriorityQueue:
    def __init__(self):
        self._queue = []
        self._index = 0

    def insert(self, item, priority):
        heapq.heappush(self._queue, (priority, self._index, item))
        self._index += 1

    def remove(self):
        a = heapq.heappop(self._queue)
        return a

    def is_empty(self):
        return len(self._queue) == 0

    def get_queue(self):
        return self._queue

    def get_size(self):
        return len(self._queue)

    def get_cost(self, key):
        for cost, _, item in self._queue:
            if item[0] == key:
                return cost
        return -1

    def __contains__(self, key):
        for _, _, item in self._queue:
            # look current state not parent
            if item[0] == key:
                return True
        return False

    def __delitem__(self, key):
        for i, (_, _, item) in enumerate(self._queue):
            if item[0] == key:
                # self._queue[i] = self._queue[-1]
                # self._queue.pop()
                # heapq.heapify(self._queue)
                self._queue.pop(i)
                break

# test...
# queue = PriorityQueue()
# queue.insert('e', 9)
# queue.insert('a', 2)
# queue.insert('h', 13)
# queue.insert('e', 5)
# queue.insert('c', 11)
# print(queue.remove())
